# Bem vindo(a)! Eu sou o Adonai Diófanes
A tecnologia para gerenciamento de repositórios, códigos, mais utilizada por mim no momento, é o [GitHub](https://www.github.com/adonaidiofanes). 

Aqui no BitBucket, utilizo mais para projetos corporativos, que corporações que presto serviço utilizam, e tenho que ter essa conta.
Você pode conhecer meus projetos pessoais no [GitHub](https://www.github.com/adonaidiofanes) e talvez me ajudar, contribuindo com códigos ou dicas.

Você consegue obter todas as minhas informações em meu site: [www.adonaidiofanes.com.br](https://www.adonaidiofanes.com.br)